﻿using Entities;
using OnlineShoping.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineShoping.Controllers
{
    public class HomeController : Controller
    {
        ShoesEntities db = new ShoesEntities();
        // GET: Image
        public ActionResult Index()
        {
            try
            {
                var lstImages = db.Items.ToList();
                return View(lstImages);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ImageModel Im)
        {
            if (Im.ImageUpload != null)
            {
                Item it = new Item();
                it.ItemName = Im.ItemName;
                it.ItemSize = Im.ItemSize;
                it.ItemBrand = Im.ItemBrand;
                it.ItemPrice = Im.ItemPrice;

                it.ItemImageName = Im.ImageUpload.FileName;
                db.Items.Add(it);
                db.SaveChanges();

                var dirPath = Server.MapPath("~/ImageUpload/");
                Directory.CreateDirectory(dirPath);
                var filepath = dirPath + Im.ImageUpload.FileName;
                Im.ImageUpload.SaveAs(filepath);

            }


            return View(Im);
        }

    }
}