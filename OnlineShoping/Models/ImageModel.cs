﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineShoping.Models
{
    public class ImageModel
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemSize { get; set; }
        public string ItemBrand { get; set; }
        public decimal ItemPrice { get; set; }
        public string ItemImageName { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }
    }
}